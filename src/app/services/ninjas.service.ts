import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
const data = require('../../assets/mock-data/ninjas.json');

@Injectable()
export class NinjasService {
  private ninjas: any[];
  private ninjasStore: BehaviorSubject<any[]>;
  constructor() {
    this.ninjas = data.ninjas;
    this.ninjasStore = new BehaviorSubject(this.ninjas);
  }

  public getNinjas() {
    return this.ninjas;
  }

  public getNinja(id): any {
    return this.ninjas.find(ninja => ninja.id === id);
  }

  public getNinjaStore(): BehaviorSubject<any[]> {
    return this.ninjasStore;
  }

  public updateNinja(id: number, key: string, value: any) {
    const index = this.ninjas.findIndex(ninja => ninja.id === id);
    this.ninjas[index][key] = value;
    this.ninjasStore.next(this.ninjas);
  }
}
