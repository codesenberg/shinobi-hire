import { Injectable } from '@angular/core';
const data = require('../../assets/mock-data/third-party-missions.json');

@Injectable()
export class MissionsService {
  private missions: any;
  constructor() {
    this.missions = data.missions;
  }

  public getMissions() {
    return this.missions;
  }

  public getMission(id: number) {
    return this.missions.find(mission => mission.id === id);
  }
}
