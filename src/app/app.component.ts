import { Component } from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';


@Component({
  selector: 'sh-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
}
