import { Component, OnInit, Input } from '@angular/core';
import { NinjasService } from '../services/ninjas.service';

@Component({
  selector: 'sh-ninja-finder',
  templateUrl: './ninja-finder.component.html',
  styleUrls: ['./ninja-finder.component.scss']
})
export class NinjaFinderComponent implements OnInit {
  @Input() public missionId: number;
  public isViewProfile = false;
  public currentProfile: any;
  constructor(private ninjas: NinjasService) {
    this.selectProfile = this.selectProfile.bind(this);
  }

  ngOnInit() {
  }

  public selectProfile(id: number) {
    this.isViewProfile = true;
    this.currentProfile = this.ninjas.getNinja(id);
  }

  public backToNinjas() {
    this.isViewProfile = false;
    this.currentProfile = undefined;
  }

  public selectNinjaForMission() {
    this.ninjas.updateNinja(this.currentProfile.id, 'current_mission', this.missionId);
    this.backToNinjas();
  }
}
