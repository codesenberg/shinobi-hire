import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NinjaFinderComponent } from './ninja-finder.component';

describe('NinjaFinderComponent', () => {
  let component: NinjaFinderComponent;
  let fixture: ComponentFixture<NinjaFinderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NinjaFinderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NinjaFinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
