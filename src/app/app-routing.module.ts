import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MissionControlComponent } from './mission-control/mission-control.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: '/view-missions', pathMatch: 'full' },
  { path: 'view-missions', component: DashboardComponent, data: { title: 'Upcoming Missions' } },
  { path: 'mission/:id', component: MissionControlComponent, data: { title: 'Mission Detail' } },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes) //contains the router where as for forChild would not contain the router
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
