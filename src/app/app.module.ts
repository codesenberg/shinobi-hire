import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { MissionControlComponent } from './mission-control/mission-control.component';
import { AppRoutingModule } from './/app-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MissionsService } from './services/missions.service';
import { MissionDetailComponent } from './mission-detail/mission-detail.component';
import { NinjaFinderComponent } from './ninja-finder/ninja-finder.component';
import { NinjasService } from './services/ninjas.service';
import { NinjaDashboardComponent } from './ninja-dashboard/ninja-dashboard.component';
import { NinjaProfileComponent } from './ninja-profile/ninja-profile.component';
import { SelectedNinjasComponent } from './mission-detail/selected-ninjas/selected-ninjas.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeaderComponent,
    MissionControlComponent,
    PageNotFoundComponent,
    MissionDetailComponent,
    NinjaFinderComponent,
    NinjaDashboardComponent,
    NinjaProfileComponent,
    SelectedNinjasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [MissionsService, NinjasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
