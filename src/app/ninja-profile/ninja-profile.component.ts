import { Component, OnInit, Input } from '@angular/core';
import { NinjasService } from '../services/ninjas.service';

@Component({
  selector: 'sh-ninja-profile',
  templateUrl: './ninja-profile.component.html',
  styleUrls: ['./ninja-profile.component.scss']
})
export class NinjaProfileComponent implements OnInit {
  @Input() public id: number;
  public profile: any;
  constructor(private ninjas: NinjasService) { }

  ngOnInit() {
    this.profile = this.ninjas.getNinja(this.id);
  }

}
