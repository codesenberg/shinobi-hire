import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NinjaProfileComponent } from './ninja-profile.component';

describe('NinjaProfileComponent', () => {
  let component: NinjaProfileComponent;
  let fixture: ComponentFixture<NinjaProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NinjaProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NinjaProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
