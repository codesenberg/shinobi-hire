import { Component, OnInit } from '@angular/core';
import { MissionsService } from '../services/missions.service';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'sh-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public heading: any[];
  public body: any[];
  constructor(private missions: MissionsService) { }

  ngOnInit() {
    this.heading = ['Id', 'Mission', 'Location', 'Rank', 'Fee'];
    this.body = this.missions.getMissions();
  }
}
