import { Component, OnInit, Input } from '@angular/core';
import { MissionsService } from '../services/missions.service';
import { NinjasService } from '../services/ninjas.service';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'sh-mission-detail',
  templateUrl: './mission-detail.component.html',
  styleUrls: ['./mission-detail.component.scss']
})
export class MissionDetailComponent implements OnInit {
  @Input() public id: number;
  public details: any;
  public ninjasHired = [];
  constructor(private missions: MissionsService, private ninjas: NinjasService) { }

  ngOnInit() {
    this.details = this.missions.getMission(this.id);
  }
}
