import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedNinjasComponent } from './selected-ninjas.component';

describe('SelectedNinjasComponent', () => {
  let component: SelectedNinjasComponent;
  let fixture: ComponentFixture<SelectedNinjasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedNinjasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedNinjasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
