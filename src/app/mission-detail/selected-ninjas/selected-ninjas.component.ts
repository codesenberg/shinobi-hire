import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NinjasService } from '../../services/ninjas.service';

@Component({
  selector: 'sh-selected-ninjas',
  templateUrl: './selected-ninjas.component.html',
  styleUrls: ['./selected-ninjas.component.scss']
})
export class SelectedNinjasComponent implements OnInit, OnDestroy {
  @Input() public id: number;
  public selectedNinjas = [];
  private ninjaSubscription: any;
  constructor(private ninjas: NinjasService) { }

  public removeFromMission(id) {
    this.ninjas.updateNinja(id, 'current_mission', '');
  }

  ngOnInit() {
    this.ninjaSubscription = this.ninjas.getNinjaStore().subscribe({
      next: (store) => this.selectedNinjas = store.filter(ninja => ninja.current_mission === this.id)
    });
  }

  ngOnDestroy() {
    this.ninjaSubscription.unsubscribe();
  }
}
