import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NinjaDashboardComponent } from './ninja-dashboard.component';

describe('NinjaDashboardComponent', () => {
  let component: NinjaDashboardComponent;
  let fixture: ComponentFixture<NinjaDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NinjaDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NinjaDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
