import { Component, OnInit, Input } from '@angular/core';
import { NinjasService } from '../services/ninjas.service';

@Component({
  selector: 'sh-ninja-dashboard',
  templateUrl: './ninja-dashboard.component.html',
  styleUrls: ['./ninja-dashboard.component.scss']
})
export class NinjaDashboardComponent implements OnInit {
  public heading: any[];
  public body: any[];
  @Input() public onProfileSelect: Function;
  constructor(private ninjas: NinjasService) { }

  ngOnInit() {
    this.heading = ['Id', 'Profile', 'Name', 'Rank', 'Available'];
    this.body = this.ninjas.getNinjas();
  }

}

